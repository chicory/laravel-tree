@foreach($categories AS $category)
    {{ $category->name }} <br />
    @if($category->childs()->count())
        <x-categories-list :categories="$category->childs" />
    @endif
@endforeach
