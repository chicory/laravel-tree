<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Contracts\View\View;

class CategoryController extends Controller
{
    /**
     * List categories.
     *
     * @param \App\Models\Category $category
     * @return \Illuminate\Contracts\View\View
     */
    public function __invoke(Category $category = null): View
    {
        $categories = Category::with(['childs'])->where('parent_id', $category?->id)->get();
        return view('categories', compact('categories'));
    }
}
