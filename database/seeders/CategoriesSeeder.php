<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Category;

class CategoriesSeeder extends Seeder
{
    use WithoutModelEvents;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // 1st level categories.
        Category::factory(3)->create();

        // 2nd level categories.
        Category::factory(3)->state([
            'parent_id' => rand(1, 3),
        ])->create();

        // 3rd level categories.
        Category::factory(3)->state([
            'parent_id' => rand(4, 6),
        ])->create();
    }
}
