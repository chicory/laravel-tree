# Laravel Categories Tree
Categories listing demo in laravel.

## System requirements
* PHP 8+
* PHP BCMath
* PHP Ctype
* PHP Fileinfo
* PHP JSON
* PHP Mbstring
* PHP OpenSSL
* PHP PDO
* PHP Tokenizer
* PHP XML
* PHP GDLib
* composer

## Installation
```bash
git clone https://codeberg.org/chicory/laravel-tree.git
cd laravel-tree
cp .env.example .env

```
Configure DB connection in `.env` file.
```bash
php artisan key:generate
php artisan migrate 
php artisan db:seed
```

## Run
```bash
php artisan serve
```

## Routes
```
/categories
/categories/{category_id}
```
`category_id` - optional parameter.
